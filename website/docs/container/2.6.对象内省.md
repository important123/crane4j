## 对象内省

在对象内省中，如果在 `@Assemble` 注解中未指定容器的 `namespace`，默认情况下会使用一个 `EmptyContainer` 作为占位符，而不会实际调用容器。相反，它会对待处理的对象进行内省操作，将待处理对象本身作为数据源。

例如：

```java
public class Foo {
    @Assemble(props = @Mapping(ref = "alias"))
    private String name;
    private String alias;
}
```

在上述示例中，通过对象内省的方式，将对象的 `name` 字段映射到 `alias` 字段上。

这个功能通常用于同步冗余的别名字段，可以通过对象内省自动将一个字段的值设置到另一个字段上，从而实现字段值的同步更新。