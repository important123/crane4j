## 自定义容器

用户可以通过实现 `Container` 接口来自定义容器，并将其声明为 `@Bean` 或 `@Component`，以便在程序中使用。

下面是一个示例：

```java
@Component
@RequiredArgsConstructor
public class UserContainer implements Container<Integer> {
    
    private final UserService userService;
    
    public String getNamespace() {
        return "user";
    }
    
    public Map<Integer, UserDO> get(Collection<Integer> ids) {
        List<UserDO> users = userService.listByIds(ids);
        return users.stream().collect(Collectors.toMap(UserDO::getId, Function.identity()));
    }
}
```

在上述示例中，我们实现了 `Container` 接口，并创建了一个根据用户ID返回`UserDO`集合的数据源容器。

通过添加 `@Component` 注解，将其交给 Spring 容器管理。随后，您可以在其他地方通过命名空间 `user` 来引用该容器。

在非 Spring 环境中，您可以手动注册该容器，方法与常量数据源容器或 lambda 表达式数据源容器相似。首先，获取 `Crane4jGlobalConfiguration` 对象，然后将自定义容器注册到该全局配置中。
