## 概述

在某些情况下，我们需要填充的对象中可能嵌套了其他对象，这种情况下，我们需要先将这些嵌套对象拆分出来，然后再进行填充操作。这个将嵌套对象取出并展开的操作称为**拆卸操作**。

嵌套对象可能存在多层级的情况，因此在执行填充操作之前，我们需要先完成拆卸操作，将所有的嵌套对象展开，然后再统一进行装配。

![拆卸操作示意图](https://img.xiajibagao.top/image-20230220182831112.png)

:::tip

拆卸操作只是为了展开嵌套对象，它是嵌套填充的第一步。

:::

## 3.4.1.在属性上声明

我们可以直接在需要进行拆卸操作的属性上使用 `@Disassemble` 注解进行声明：

```java
public class Department {
    private Integer id;
    @Disassemble(type = Employee.class)
    private List<Employee> employees;
}
```

在上面的示例中，对于填充 `Department` 对象之前，会先将 `Department` 中的所有 `Employee` 对象取出并展开。如果 `Employee` 对象中还存在需要拆卸的嵌套对象，也会一并取出并展开，一直递归下去，直到所有的对象都被展开为止。

拆卸操作支持处理数组、集合 (`Collection`) 或单个对象。

:::tip

和 `@Assemble` 一样，可以在属性上重复声明 `@Disassemble` 注解，但这样做没有实际意义。

:::

## 3.4.2.在类上声明

与 `@Assemble` 注解一样，我们也可以将 `@Disassemble` 注解声明在类上：

```java
// 直接声明
@Disassemble(key = "employees", type = Employee.class)
public class Department {
    private Integer id;
    private List<Employee> employees;
}
```

当在类级别上声明 `@Disassemble` 注解时，需要使用 `key` 属性指定需要拆卸的字段。

## 3.4.3.自动类型推断

在某些情况下，无法在编译期确定要填充的对象类型。此时，可以不指定 `type` 属性，而是在执行拆卸操作时动态推断类型：

```java
public class Department<T> {
    private Integer id;
    @Disassemble // 无法确定填充类型
    private List<T> employees;
}
```

上述示例中，无法在编译期确定 `employees` 属性的类型，因此没有指定 `type` 属性。在执行拆卸操作时，会动态推断 `employees` 属性的类型。

这个功能是通过类型解析器 `TypeResolver` 实现的。用户可以实现 `TypeResolver` 接口来替换默认的类型解析器，以适应特定的需求。

## 3.4.4.拆卸操作处理器

与装配操作类似，拆卸操作也依赖于拆卸操作处理器 `DisassembleOperationHandler` 来完成。用户可以在注解中使用 `handler` 或 `handlerType` 属性来指定要使用的处理器。

例如：

```java
public class Department {
    private Integer id;
    @Disassemble(
        type = Employee.class,
        handlerType = ReflectiveDisassembleOperationHandler.class // 指定操作处理器
    )
    private List<Employee> employees;
}
```

在配置解析过程中，会根据指定的类型和处理器类型获取对应的操作处理器。用户可以根据自己的需求，实现自定义的拆卸操作处理器，并通过 `handler` 或 `handlerType` 属性进行指定。

目前默认的，也是唯一的拆卸操作处理就是 `ReflectiveDisassembleOperationHandler`，因此一般可以不用指定。
